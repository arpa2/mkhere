#!/bin/bash
#
# gitea build script (light-weight, self-hosted Git service)

. $(dirname "$0")/lib/stdlib

default_VERSION 1.16.8
default_PLATFORM=linux-amd64

PLATFORM=${default_PLATFORM}

do_update () {
	# This is TERRIBLE because it downloads a binary
	# Note that there also is a .asc but that adds little: the download site is validated
	cd "$DIR_FETCH"
	if [ ! -r "gitea-$VERSION-$PLATFORM" ]
	then
		wget "https://dl.gitea.io/gitea/$VERSION/gitea-$VERSION-$PLATFORM"
	fi
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	# Building gitea requires very recent Go, NodeJS
	# This does not make it practical to build on arbitrary platforms
	# And yes, this is a soggy approach for open source software
	mkdir -p "$DIR_TREE/usr/local/sbin"
	cp "$DIR_FETCH/gitea-$VERSION-$PLATFORM" "$DIR_TREE/usr/local/sbin/gitea"
	chmod ugo+x "$DIR_TREE/usr/local/sbin/gitea"
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

