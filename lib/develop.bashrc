# Internationalisation
#
export LANG=C
export LC_ALL=C

# Wrap the "make" command and insert DESTDIR=/tree/... for the current package
#
make () {
        PKG=$(pwd | sed -E "s=^($HOME|[~])//*([^/.]*)[.](dl|src|build|tree)(/.*|$)$=\2=" | sed 's/^[/].*//')
        if [ -z "$PKG" ]
        then
                echo 2>&1 'Please run make from any package directory, so we can infer DESTDIR'
                return 1
        else
                DESTDIR=$(readlink -f "$HOME/$PKG.tree")
		BUILDIR=$(readlink -f "$HOME/$PKG.build")
                echo "Inserting -C $BUILDIR DESTDIR=$DESTDIR"
                $(which make) -C "$BUILDIR" "DESTDIR=$DESTDIR" "$@"
        fi
}

# Wrap the "cmake" command and insert DESTDIR=/tree/... for the current package
#
cmake () {
        PKG=$(pwd | sed -E "s=^($HOME|[~])//*([^/.]*)[.](src)(/.*|$)$=\2=" | sed 's/^[/].*//')
        if [ -z "$PKG" ]
        then
		$(which cmake) "$@"
	else
                echo 2>&1 "Please do not run cmake from the $PKG source directory"
                return 1
        fi
}

# Wrap the "git" command and insert -C=/src/... for the current package
#
git () {
        PKG=$(pwd | sed -E "s=^($HOME|[~])//*([^/.]*)[.](dl|build|tree)(/.*|$)$=\2=" | sed 's/^[/].*//')
        if [ -z "$PKG" ]
        then
                $(which git) "$@"
                return 1
        else
                GITDIR=$(readlink -f "$HOME/$PKG.src")
                echo "Inserting -C ~/$PKG.src"
                $(which git) -C "$GITDIR" "$@"
        fi
}

# Wrap the "tig" command and insert -C/src/... for the current package
#
tig () {
        PKG=$(pwd | sed -E "s=^($HOME|[~])//*([^/.]*)[.](dl|build|tree)(/.*|$)$=\2=" | sed 's/^[/].*//')
        if [ -z "$PKG" ]
        then
                $(which tig) "$@"
                return 1
        else
                GITDIR=$(readlink -f "$HOME/$PKG.src")
                echo "Inserting -C ~/$PKG.src"
                $(which tig) -C "$GITDIR" "$@"
        fi
}

# View the tree of processes below CTest, useful when it's stuck
#
ctree() {
	for pid in $(ps -C ctest -o pid=)
	do
		pstree -p $pid
	done | less -F
}

# Assuming CTest/Pypeline/... tests, kill those ... and finally Pypeline too
#
ckill() {
	PIDS="$@"
	if [ -z "$PIDS" ]
	then
		for pid_ctest in $(ps -C ctest -o pid=)
		do
			if [ -n "$PIDS" ]
			then
				echo >&2 'Ambiguous killing is not nice.  Be explicit.'
				return
			else
				PIDS="$pid_ctest"
			fi
		done
	fi
	for pid_ctest in $PIDS
	do
		for pid_pypeline in $(ps -p $pid_ctest -o pid=)
		do
			for pid_test in $(ps -p $pid_pypeline -o pid=)
			do
				kill $pid_test
			done
			sleep 1
			kill $pid_pypeline 2>/dev/null || true
		done
	done
}

# Autarkian run, by default on all dependencies
#
autarkian () {
	if [ $# -eq 0 ]
	then
		/mkhere/bin/autarkian ~/*.build
	else
		/mkhere/bin/autarkian "$@"
	fi
}
