#!/bin/bash
#
# ARPA2 SteamWorks -- LDAP-derived service configuration utilities

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/steamworks.git"

default_VERSION v0.97.2

do_dependencies () {
	echo arpa2cm
}

do_osdependencies () {
	echo liblog4cpp5-dev
	echo libldap-dev
	echo libsqlite3-dev
	# echo catch2
}

do2_build () {
	do2cmake_build -DDEBUG:BOOL=ON -DLOG_STYLE:STRING=LOG_STYLE_STDERR
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_test () {
	# Cannot run network tests (nor exclude them with an option yet)
	return 0
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

