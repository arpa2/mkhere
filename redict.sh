#!/bin/bash
#
# redict build script

. $(dirname "$0")/lib/stdlib

default_VERSION 7.3.0

do_update () {
	cd "$DIR_FETCH"
	wget -O redict-${VERSION}.tar.gz https://codeberg.org/redict/redict/archive/${VERSION}.tar.gz
	cd "$DIR_SRC"
	empty_dir "$DIR_SRC/redict-${VERSION}"
	tar --strip-components=1 -xzvf "$DIR_FETCH/redict-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	cp -r "$DIR_SRC"/* "$DIR_BUILD/"
	# "$DIR_SRC/configure" --prefix="/usr" && \
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

