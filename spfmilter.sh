#!/bin/bash
#
# spfmilter build script

. $(dirname "$0")/lib/stdlib

default_VERSION 2.001

do_update () {
	cd "$DIR_FETCH"
	wget https://www.acme.com/software/spfmilter/spfmilter-${VERSION}.tar.gz
	cd "$DIR_SRC"
	empty_dir "$DIR_SRC/spfmilter-${VERSION}"
	tar -xzvf "$DIR_FETCH/spfmilter-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo libspf2-dev
	echo libmilter-dev
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	#IGNORANT_OF_DATADIR#
	( "$DIR_SRC/spfmilter-${VERSION}/configure" || true ) && \
	#IGNORANT_OF_BUILDDIR#
	cp "$DIR_SRC/spfmilter-${VERSION}"/*.[ch8] "$DIR_BUILD" && \
	make && \
	empty_dir "$DIR_TREE" && \
	#IGNORANT_OF_DESTDIR# make DESTDIR="$DIR_TREE" install
	make -n install | sed "s+ /usr/+ $DIR_TREE/usr/+g" | sh
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

