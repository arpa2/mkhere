#!/bin/bash
#
# bogofilter build script

. $(dirname "$0")/lib/stdlib

default_VERSION 1.2.5

do_update () {
	cd "$DIR_FETCH"
	wget -O bogofilter-${VERSION}.tar.xz https://sourceforge.net/projects/bogofilter/files/bogofilter-stable/bogofilter-${VERSION}.tar.xz/download
	cd "$DIR_SRC"
	empty_dir "$DIR_SRC/bogofilter-${VERSION}"
	tar -xJvf "$DIR_FETCH/bogofilter-${VERSION}.tar.xz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	"$DIR_SRC/bogofilter-${VERSION}/configure" --prefix="/usr" && \
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

