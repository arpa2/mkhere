#!/bin/bash
#
# acmetool build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://github.com/hlandau/acmetool"

default_VERSION v0.2.1

do_update () {
	do2git_update "$@"
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo golang
	echo golang-go
	echo libcap-dev
}

do2_build () {
	cd "$DIR_SRC"
	# Make the directory for acmetool, otherwise you'll have a program named /usr/local/bin :-D
	mkdir -p "$DIR_TREE/usr/local/bin"
	make && make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

