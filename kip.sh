#!/bin/bash
#
# KIP and HAAN build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v0.13.5
GIT_URL="https://gitlab.com/arpa2/kip.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
DOCS_BUILD="doc/html"

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	echo quickder
	echo quicksasl
	echo freediameter
}

do_osdependencies () {
	echo libkrb5-dev
	# libkrb5-dev does not [always] depend on libkrb5
	echo libkrb5-3
	echo libunbound-dev
	echo libsasl2-dev
	echo sasl2-bin
	echo libev-dev
	echo libssl-dev
	echo libpcre3-dev
	echo libjson-c-dev
	echo libfcgi
	echo libfcgi-dev
	echo libjson-c-dev
	echo bison
	echo flex
	echo swig
	echo python3
	echo python3-cbor
	echo python3-dev
	echo python3-setuptools
}

do2_build () {
	setup_destdirs freediameter quicksasl
	do2cmake_build -DfreeDiameter_INCLUDE_DIR=$DESTDIR_freediameter/usr/local/include/freeDiameter -DfreeDiameter_EXECUTABLE=$DESTDIR_freediameter/usr/local/bin/freeDiameterd -DfreeDiameter_EXTENSION_DIR=$DESTDIR_freediameter/usr/local/lib/freeDiameter -DfreeDiameter_fdcore_LIBRARY=$DESTDIR_freediameter/usr/local/lib/libfdcore.so -DfreeDiameter_fdproto_LIBRARY=$DESTDIR_freediameter/usr/local/lib/libfdproto.so -DCONTRIB_CYRUSSASL2_HAAN:BOOL=ON -DCONTRIB_FASTCGI_HAAN:BOOL=ON -DDEBUG:BOOL=ON -DLOG_STYLE:STRING=LOG_STYLE_STDERR #TODO# && \
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

do_test () {
	setup_destdirs freediameter
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DESTDIR_freediameter/usr/local/lib/freeDiameter
	do2cmake_test "$@"
}

main_do_commands "$@"

