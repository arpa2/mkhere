#!/bin/bash
#
# Pavlov build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v0.2.0

GIT_URL="https://gitlab.com/arpa2/pavlov.git"
DOCS_BUILD="html"

do_dependencies () {
	echo arpa2cm
	echo arpa2common
}

do_osdependencies () {
	echo -n 'libcom-err2'
}

do2_build () {
	do2cmake_build
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

