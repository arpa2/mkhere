#!/bin/bash
#
# Open Container Initiative's RunC build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://github.com/opencontainers/runc"

# Trouble with Debian go for 1.1.0 (lovely, these new instable langauges)
# default_VERSION v1.1.0
default_VERSION v1.0.3

do_update () {
	do2git_update "$@"
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo golang
	echo golang-go
	echo libcap-dev
	echo libseccomp-dev
}

do2_build () {
	cd "$DIR_SRC"
	make && make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

