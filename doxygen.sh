#!/bin/bash
#
# Quick DER build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://github.com/doxygen/doxygen"

default_VERSION Release_1_8_20

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo graphviz
	echo mscgen
	echo python3
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

