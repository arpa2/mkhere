#!/bin/bash
#
# freeDiameter build script -- from the official repo on GitHub

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION 1.5.0
GIT_URL="https://github.com/freediameter/freediameter.git"
# BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
DOCS_BUILD="doc/html"

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo gnutls-dev
	echo libsctp-dev
	echo libgcrypt-dev
	echo libidn11-dev
	# echo libgnutls30
	echo bison
	echo flex
}

do2_build () {
	do2cmake_build -DCMAKE_BUILD_TYPE:STRING=Debug
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

