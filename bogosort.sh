#!/bin/bash
#
# bogosort build script (ARPA2 adaptations of bogofilter, for sorting messages over multiple aliases)

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://gitlab.com/arpa2/bogosort"

default_VERSION bogofilter-1.2.5

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo autoconf
	echo automake
	echo flex
	echo xmlto
}

do2_build () {
	cd "$DIR_SRC/bogofilter" && \
	autoreconf -i -s -f && \
	cd "$DIR_BUILD" && \
	empty_dir && \
	"$DIR_SRC/bogofilter/configure" --prefix=/usr/local && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

