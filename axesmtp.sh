#!/bin/bash
#
# Axe_SMTP build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/axesmtp.git"
DOCS_BUILD="doc/html"

default_VERSION v1.7.1

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	# For testing:
	echo pavlov
}

do_osdependencies () {
	echo libev-dev
	echo libssl-dev
	echo libev-dev
	echo libfcgi
	echo libfcgi-dev
	echo doxygen
	echo graphviz
}

do2_build () {
	do2cmake_build -DCONTRIB_MAKEMAIL_FASTCGI=ON -DCONTRIB_FORMMAIL_FASTCGI=ON # && \
	# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

