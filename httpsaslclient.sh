#!/bin/bash
#
# ARPA2 Apache browser plugin and native client

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/http_sasl_client.git"
DOCS_BUILD="doc/html"

default_VERSION v0.1.0

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	echo quickder
	echo quicksasl
	echo kip
	echo apachemod
}

do_osdependencies () {
	echo automake
	echo libtool-bin
	echo libpcre3-dev
	echo liblmdb-dev
	echo lmdb-utils
	echo apache2
	echo apache2-dev
	echo apache2-utils
	echo libsasl2-dev
	echo sasl2-bin
	echo curl
	echo libcurl4-openssl-dev
	echo tcpdump
	echo qtbase5-dev
	echo qtdeclarative5-dev
	echo qml-module-qtquick2
	echo qml-module-qtquick-controls2
	echo qml-module-qtquick-layouts
	echo qml-module-qtquick-window2
	echo firefox-esr
	echo python3-selenium
	echo freediameter
	echo freediameter_sasl
}

do2_build () {
	setup_destdirs freediameter freediameter_sasl kip apachemod
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DESTDIR_freediameter/usr/local/lib/freeDiameter:$DESTDIR_freediameter_sasl/usr/local/lib/freeDiameter
	export PATH=$PATH:$DESTDIR_kip/usr/local/sbin
	cd "$DIR_BUILD"
	empty_dir
	cmake "$DIR_SRC" -DCMAKE_INSTALL_PREFIX="$DIR_TREE" "$@"
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

do_test () {
	setup_destdirs freediameter freediameter_sasl apachemod
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DESTDIR_freediameter/usr/local/lib:$DESTDIR_freediameter/usr/local/lib/freeDiameter:$DESTDIR_freediameter_sasl/usr/local/lib/freeDiameter
	echo $LD_LIBRARY_PATH
	export PATH=$PATH:$DESTDIR_kip/usr/local/sbin
	do2cmake_test "$@"
}

main_do_commands "$@"
