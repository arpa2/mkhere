#!/bin/bash
#
# SIPproxy64 build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

# GIT_URL="git://git.code.sf.net/p/sipproxy64/code"
# GIT_URL="https://git.code.sf.net/p/sipproxy64/code"
GIT_URL="https://gitlab.com/0cpm/sipproxy64"

default_VERSION v0.10

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

