#!/bin/bash
#
# matrix_synapse build script

. $(dirname "$0")/lib/stdlib

default_VERSION 1.25.0

do_update () {
	. $(dirname "$0")/lib/pkglib pip3
	pkg_update
}

do_dependencies () {
	echo -n ''
	# echo pypackages
}

do_osdependencies () {
	echo sqlite3
	echo libssl-dev
	echo virtualenv
	echo libjpeg-dev
	echo libxslt1-dev
	echo python3-distutils
}

do2_build () {
	. $(dirname "$0")/lib/pkglib pip3
	pkg_install matrix-synapse
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

