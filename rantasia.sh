#!/bin/bash
#
# RANtasia build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/RANtasia.git"

default_VERSION v0.5.2

do_dependencies () {
	echo arpa2cm
	echo quickmem
}

do_osdependencies () {
	echo libcap-ng-dev
	echo libev-dev
}

do2_build () {
	do2cmake_build # && \
	# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

