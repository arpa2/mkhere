#!/bin/bash
#
# conserver build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

default_VERSION d78a55ff3725ca9f835b02b5d1739d4c972746ed
#Note: May need to update to git tag of master

GIT_URL="https://gitlab.com/Vrancken/gnutls"

do_dependencies () {
	echo nettle
}

do_osdependencies () {
	echo gnulib
	echo gperf
	echo autogen
	echo autotools-dev
	echo libgmp-dev
	# echo nettle-dev
	echo libtasn1-dev
	echo libp11-kit-dev
	echo libtspi-dev
	echo libunbound-dev
	echo libz-dev
	# echo libidn-dev
	echo libidn2-dev
}

#
# GnuTLS is a package all on its own for building...
#
# NEVER CLONE THIS RECIPE, IT IS HIGHLY SPECIALISED CRUFT
#
# Subdirectory "work" encapsulates dot files so empty_dir works
#
# This recipe also corrects for erroneous library order in
# GnuTLS's configure output: when supplied with a manually
# overridden Nettle as we do, it still ranks OS libraries
# first.
#
# This is terrible, I know.  I had to tweak *so* much that
# it freaks me out.  I'm not building documentation below
# because it introduces yet another linker error!
#
do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	git clone --depth=1 "file://$DIR_SRC" "$DIR_BUILD/work" && \
	cd work && \
	for TOOL in psktool certtool srptool danetool gnutls_cli gnutls_cli_debug gnutls_serv psktool systemkey danetool ocsptool p11tool tpmtool
	do
		sed -i -e "s/^${TOOL}_LDADD *=/${TOOL}_LDADD = @NETTLE_LIBS@ @HOGWEED_LIBS@/" src/Makefile.am
	done && \
	for TOOL in libcmd_certtool_la libcmd_danetool_la libcmd_systemkey_la libcmd_p11tool_la libcmd_tpmtool_la
	do
		sed -i -e "s/^${TOOL}_LIBADD *=/${TOOL}_LIBADD = @NETTLE_LIBS@ @HOGWEED_LIBS@/" src/Makefile.am
	done && \
	# sed -i -e 's/^COMMON_LDADD *=/COMMON_LDADD = @NETTLE_LIBS@ @HOGWEED_LIBS@/' tests/Makefile.am
	find tests doc -name Makefile.am | while read AMFILE
	do
		sed -i -e   's/_LDADD =/_LDADD = @NETTLE_LIBS@ @HOGWEED_LIBS@/' "$AMFILE"
		sed -i -e 's/_LIBADD =/_LIBADD = @NETTLE_LIBS@ @HOGWEED_LIBS@/' "$AMFILE"
	done && \
	echo >> doc/examples/Makefile.am 'LDADD += @NETTLE_LIBS@ @HOGWEED_LIBS@'
	./bootstrap && \
	sed -i ./configure \
		-e 's/^LIBS="\$LIBS \$NETTLE_LIBS"$/LIBS="\$NETTLE_LIBS \$LIBS"/' \
		-e 's/^LIBS="\$LIBS \$HOGWEED_LIBS \$NETTLE_LIBS"/LIBS="\$HOGWEED_LIBS \$NETTLE_LIBS \$LIBS"/' \
		-e 's/^LIBS="-lhogweed \$HOGWEED_LIBS \$NETTLE_LIBS \$LIBS"/LIBS="\$HOGWEED_LIBS \$NETTLE_LIBS \$LIBS"/' && \
	NETTLE_LIBS=/tree/nettle/usr/local/lib64/libnettle.so \
	NETTLE_CFLAGS=-I/tree/nettle/usr/local/include \
	HOGWEED_LIBS=/tree/nettle/usr/local/lib64/libhogweed.so \
	HOGWEED_CFLAGS=-I/tree/nettle/usr/local/include \
		./configure --prefix="/usr/local" --with-included-unistring --disable-doc && \
	NETTLE_LIBS=/tree/nettle/usr/local/lib64/libnettle.so \
	NETTLE_CFLAGS=-I/tree/nettle/usr/local/include \
	HOGWEED_LIBS=/tree/nettle/usr/local/lib64/libhogweed.so \
	HOGWEED_CFLAGS=-I/tree/nettle/usr/local/include \
		make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	cd "$DIR_BUILD/work"
	echo 'SKIPPED: It is known to fail on grounds of mere test correctness'
	# make check
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

