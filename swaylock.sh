#!/bin/bash
#
# swaylock build script (Desktop locking for Sway WM)
#
#TODO# Requirement of silly high meson number (beyond Debian stable)

. $(dirname "$0")/lib/stdlib

default_VERSION v1.7.2

do_update () {
	cd "$DIR_SRC"
	empty_dir
	git clone https://github.com/swaywm/swaylock "$DIR_GIT"
	cd "$DIR_GIT" ; git reset $VERSION
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo meson
	echo wayland
	echo wayland-protocols
	echo libxkbcommon
	echo cairo
	echo gdk-pixbuf2
	echo pam
	echo scdoc
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	meson --buildtype debugoptimized "$DIR_BUILD" "$DIR_GIT"
	ninja all && \
	empty_dir "$DIR_TREE" && \
	DESTDIR="$DIR_TREE" ninja install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

