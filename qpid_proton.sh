#!/bin/bash
#
# Apache Qpid Proton build script
#
# Note: We need a fix after installing ProtonConfig.cmake files; these
#       contain fixed file/dir names such as /usr/local/include and we
#       need them prefixed with $DIR_TREE at build time.  The setup of
#       these .cmake files seems geared towards installation before
#       loading them into the next build project.  In comparison, the
#       ARPA2CM package has a clever trick with relative directories.
#       We fix locally with "sed -i" and continue to build qpid_dispatch.

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolcmake

default_VERSION 0.34.0

do_update () {
	cd "$DIR_FETCH"
	empty_dir
	wget "https://github.com/apache/qpid-proton/archive/refs/tags/${VERSION}.tar.gz"
	cd "$DIR_SRC"
	empty_dir
	tar --strip-components=1 -xzvf "$DIR_FETCH/${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo libsasl2-dev
	echo libssl-dev
	echo python3
	echo libpython3-dev
}

do2_build () {
	# Build, then .cmake with fixed target names (insert $TREEDIR)
	do2cmake_build -DBUILD_TESTING=OFF &&
	find "$DIR_TREE" -name \*.cmake -exec \
		sed -i "s+/usr/+${DIR_TREE%/}/usr/+g" {} \;
}

do_test () {
	# Skip; there are currently problems with the tests
	# Note: We also set -DBUILD_TESTING=OFF in do2_build()
	return 0
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

