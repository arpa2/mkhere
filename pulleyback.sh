#!/bin/bash
#
# ARPA2 Pulleyback for SteamWorks -- LDAP-derived service configuration utilities

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/steamworks-pulleyback.git"
DOCS_BUILD="doc/html"

default_VERSION v0.3.0

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo steamworks
}

do_osdependencies () {
	echo liblog4cpp5-dev
	echo libldap-dev
	echo libsqlite3-dev
	echo liblua5.2-dev
	echo uuid-dev
	echo lua5.3
	echo slapd
	echo ldap-utils
	echo doxygen
	# echo catch2
}

do2_build () {
	do2cmake_build -DDEBUG:BOOL=ON -DLOG_STYLE:STRING=LOG_STYLE_STDERR #TODO# && \
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
	# Crudely correct nesting of DESTDIRs between packages
	cp -pr "$DIR_TREE"/tree/steamworks*/* "$DIR_TREE" && \
	rm -rf "$DIR_TREE"/tree/steamworks* && \
	rmdir "$DIR_TREE"/tree
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

