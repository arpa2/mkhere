#!/bin/bash
#
# LibVNC client/server

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

#
# Cloned to ARPA2 so we can work on it
#
default_VERSION LibVNCServer-0.9.13
GIT_URL="https://github.com/arpa2/libvncserver"
BIN_URL="https://github.com/arpa2/libvncserver/archive/refs/tags/${VERSION}.tar.gz"
# DOCS_BUILD="doc/html"

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	echo quickder
	echo quicksasl
}

do_osdependencies () {
	return 0
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

