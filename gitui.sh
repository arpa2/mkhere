#!/bin/bash
#
# gitui build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="git://github.com/Extrawurst/gitui"

default_VERSION 3b5d43ecb28d4846e4f5d16b3fa68ab63fd776c9

do_update () {
	do2git_update "$@"
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo rustc
	echo cargo
}

do2_build () {
	cd "$DIR_SRC"
	# Make the directory for acmetool, otherwise you'll have a program named /usr/local/bin :-D
	mkdir -p "$DIR_TREE/usr/local/bin"
	make && make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

