#!/bin/bash
#
# ARPA2 freediameter diasasl-client.fdx and diasasl-server.fdx
#

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/freediameter-sasl.git"
DOCS_BUILD="doc/html"

default_VERSION v1.0.7

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	echo quickder
	echo quicksasl
	echo freediameter
	echo kip
}

do_osdependencies () {
	echo automake
	echo libtool-bin
	echo libpcre3-dev
	echo liblmdb-dev
	echo lmdb-utils
	echo apache2
	echo apache2-dev
	echo apache2-utils
	echo libsasl2-dev
	echo sasl2-bin
	echo curl
	echo libcurl4-openssl-dev
}

do2_build () {
	setup_destdirs freediameter
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DESTDIR_freediameter/usr/local/lib/freeDiameter
	cd "$DIR_BUILD"
	empty_dir
	cmake "$DIR_SRC" "$@"
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

do_test () {
	setup_destdirs freediameter kip
	export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DESTDIR_freediameter/usr/local/lib/freeDiameter
	echo $LD_LIBRARY_PATH
	export PATH=$PATH:$DESTDIR_kip/usr/local/sbin
	do2cmake_test "$@"
}

main_do_commands "$@"
