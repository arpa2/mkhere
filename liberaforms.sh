#!/bin/bash
#
# liberaforms build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://gitlab.com/liberaforms/liberaforms"

default_VERSION c32ae20c2934457f78364bf662ad7873de0278ce

do_update () {
	do2git_update "$@"
	cd "$DIR_SRC"
}

do_dependencies () {
	echo -n ''
	# echo pypackages
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	empty_dir "$DIR_TREE"
	. $(dirname "$0")/lib/pkglib pip3
	pkg_install -r "$DIR_SRC/requirements.txt"
	mkdir -p "$DIR_TREE/var/www/liberaforms" && \
	cp -r "$DIR_SRC/"* "$DIR_TREE/var/www/liberaforms/"
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

