#!/bin/bash
#
# foreign .deb build/export script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

default_VERSION 0.0.0


if [ -z "$FLAVOUR_foreigndeb" ]
then
	echo >&2 "$0 needs FLAVOUR_foreigndeb with space-separated .deb URLs"
	exit 1
fi


do_update () {
	for _PKG in $FLAVOUR_foreigndeb
	do
		_PKGLOC="${_PKG#https://}"
		if [ "$_PKG" == "$_PKGLOC" ]
		then
			#TODO# Still missing out on "/../" paths
			echo >&2 "$0 needs a https:// URL for $_PKG"
			return 1
		fi
		mkdir -p $(dirname "$DIR_FETCH/$_PKGLOC") &&
		wget -O "$DIR_FETCH/$_PKGLOC" "$_PKG"
		if [ $? -ne 0 ]
		then
			return 1
		fi
	done
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

# As long as Knot's Python code does not clone during ./configure
# and fails to incorporate the libknot/control.py we need cpio below
do2_build () {
	cd "$DIR_TREE"
	for _PKG in $FLAVOUR_foreigndeb
	do
		_PKGLOC="${_PKG#https://}"
		if [ "$_PKG" == "$_PKGLOC" ]
		then
			echo >&2 "$0 needs a https:// URL for $_PKG"
			return 1
		fi
		ar p "$DIR_FETCH/$_PKGLOC" data.tar.xz | tar -xJvf -
		if [ $? -ne 0 ]
		then
			return 1
		fi
	done
}

do_test () {
	return 0;
}

do_list () {
	for _PKG in $FLAVOUR_foreigndeb
	do
		_PKGLOC="${_PKG#https://}"
		if [ "$_PKG" == "$_PKGLOC" ]
		then
			echo >&2 "$0 needs a https:// URL for $_PKG"
			return 1
		fi
		ar p "$DIR_FETCH/$_PKGLOC" data.tar.xz | tar -tJf - | sed -e 's+^[.]/++' -e '/^$/d'
	done
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

