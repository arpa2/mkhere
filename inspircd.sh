#!/bin/bash
#
# inspIRCd build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://github.com/inspircd/inspircd"

default_VERSION v4.0.0a2

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	cd "$DIR_BUILD" && \
	empty_dir && \
	BUILD_UID=root BUILD_GID=root && \
	if $(grep -q '^inspircd:' /etc/passwd)
	then
		BUILD_UID=inspircd
	else
		BUILD_UID=root
	fi
	if $(grep -q '^inspircd:' /etc/group)
	then
		BUILD_GID=inspircd
	else
		BUILD_GID=root
	fi
	if [ "$BUILD_UID" = "root" -o "$BUILD_GID" = "root" ]
	then
		echo >&2 "Imperfectly building for $BUILD_UID:$BUILD_GID operation"
		echo >&2 "To fix thi, have inspircd:inspircd on the build system"
		sleep 3
	fi
	"$DIR_SRC/configure" --prefix=/usr/local --uid=$BUILD_UID --gid=$BUILD_UID && \
	make -C "$DIR_SRC" DESTDIR="$DIR_TREE" install
}

do_test () {
	cd "$DIR_BUILD" && \
	make test
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

