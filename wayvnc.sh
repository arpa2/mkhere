#!/bin/bash
#
# wayvnc build script (Export VNC for a [headless] wlroots session)

. $(dirname "$0")/lib/stdlib

default_VERSION v0.6.2

do_update () {
	cd "$DIR_SRC"
	empty_dir
	git clone https://github.com/any1/wayvnc "$DIR_GIT"
	cd "$DIR_GIT" ; git reset $VERSION
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo meson
	echo ninja-build
	echo pkg-config
	echo libaml-dev
	echo libdrm-dev
	echo libgbm-dev
	echo libxkbcommon-dev
	echo neatvnc
	echo libpam-dev
	echo libpixman-1-dev
	echo libjansson-dev
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	meson --buildtype debugoptimized "$DIR_BUILD" "$DIR_GIT"
	ninja all && \
	empty_dir "$DIR_TREE" && \
	DESTDIR="$DIR_TREE" ninja install
}

do_test () {
	cd "$DIR_BUILD"
	meson test
	./test/integration/integration.sh
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

