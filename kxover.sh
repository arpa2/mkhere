#!/bin/bash
#
# KXOVER build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/kxover.git"

default_VERSION v0.8.0
default_VARIANT faketls

do_dependencies () {
	echo arpa2cm
	echo quickder
	echo tlspool
}

do_osdependencies () {
	echo libcom-err2
	echo libkrb5-dev
	echo libunbound-dev
	echo libev-dev
	# echo python3
}

do2_build () {
	CMAKE_ARGS=''
	case $VARIANT in
	faketls)
		CMAKE_ARGS="${CMAKE_ARGS:+$CMAKE_ARGS }-D FAKE_TLS=TRUE"
		;;
	*)
		;;
	esac
	do2cmake_build $CMAKE_ARGS #TODO# && \
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_test () {
	# No online testing in "mkhere"
	return 0
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
	echo -n 'faketls'
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

