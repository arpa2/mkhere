#!/bin/bash
#
# mbusc build script

. $(dirname "$0")/lib/stdlib

default_VERSION v0.1

do_update () {
	cd "$DIR_SRC"
	empty_dir
	git clone https://gitlab.com/groengemak/mbusc "$DIR_GIT"
	cd "$DIR_GIT" ; git reset $VERSION
}

do_dependencies () {
	echo mbusd
}

do_osdependencies () {
	echo libev-dev
	echo libinih-dev
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	cmake -D CMAKE_INSTALL_PREFIX:PATH="/usr" "$DIR_GIT"
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

