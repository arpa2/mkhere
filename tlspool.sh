#!/bin/bash
#
# TLS Pool build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/tlspool.git"

default_VERSION v0.9.6

do_dependencies () {
	echo arpa2cm
	echo quickder
}

do_osdependencies () {
	echo libdb-dev
	echo libgnutls28-dev
	echo libgnutls-dane0
	echo libkrb5-dev
	echo libldns-dev
	echo libtasn1-dev
	echo libldap2-dev
	echo libp11-kit-dev
	echo libunbound-dev
}

do2_build () {
	do2cmake_build #TODO# && \
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

