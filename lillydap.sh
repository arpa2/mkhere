#!/bin/bash
#
# LillyDAP build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v1.0.0
GIT_URL="https://gitlab.com/arpa2/lillydap.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
# DOCS_BUILD="doc/html"

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem
	echo quickder
}

do_osdependencies () {
	echo gperf
}

do2_build () {
	do2cmake_build #TODO# && \
	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

