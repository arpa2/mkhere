#!/bin/bash
#
# lcxterm build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

default_VERSION v0.9
# You may want to use a GIT hash from master

GIT_URL="https://gitlab.com/klamonte/lcxterm"

do_update_BUG_IN_DOWNLOAD () {
	# There is not Makefile.in in the v0.9 tarball
	cd "$DIR_FETCH"
	wget -O lcxterm-${VERSION}.tar.gz "https://gitlab.com/klamonte/lcxterm/-/archive/${VERSION}/lcxterm-${VERSION}.tar.gz"
	cd "$DIR_SRC"
	empty_dir "$DIR_SRC"
	cd ..
	tar --one-top-level="lcxterm-${VERSION}" -xzvf "$DIR_FETCH/lcxterm-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo 'ncurses-dev'
	echo 'libgpm-dev'
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	# ./autogen.sh && \
	# ./configure --prefix="/usr" && \
	"$DIR_SRC/configure" --srcdir="$DIR_SRC" --prefix="/usr" && \
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

