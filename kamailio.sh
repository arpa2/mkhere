#!/bin/bash
#
# Kamailio build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://github.com/arpa2/kamailio"

# Trouble with Debian go for 1.1.0 (lovely, these new instable langauges)
# default_VERSION v1.1.0
default_VERSION 5.8.2

do_update () {
	do2git_update "$@"
}

do_dependencies () {
	true
}

do_osdependencies () {
	echo libxml2-dev
	echo libexpat1-dev
	echo libpcre2-dev
	echo libjansson-dev
	echo libsqlite3-dev
	echo libssl-dev
	echo libsctp-dev
	# For testing: sipsak, sip-tester --> sipp
	echo sipsak
	echo sip-tester
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir "$DIR_BUILD"
	# cp -r "$DIR_SRC"/* .
	git clone "$DIR_SRC" "$DIR_BUILD"
	make include_modules="db_sqlite tls cplc sctp" cfg && \
	make all && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return
	# Tests are a bit flaky, I found
	cd "$DIR_BUILD/test"
	for d in $(ls "$DIR_BUILD/test")
	do
		if [ -d "$DIR_BUILD/test/$d" -a -r "$DIR_BUILD/test/$d/Makefile" ]
		then
			make -C "$DIR_BUILD/test/$d" all
			# Cannot find ../../Makefile.modules
			# cd "$DIR_BUILD/test"
			# make -C "$d" test
		fi
	done
	make test
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

