#!/bin/bash
#
# nettle build script

. $(dirname "$0")/lib/stdlib

default_VERSION 3.7

do_update () {
	cd "$DIR_FETCH"
	wget https://ftp.gnu.org/gnu/nettle/nettle-${VERSION}.tar.gz
	cd "$DIR_SRC"
	empty_dir
	cd ..
	tar --one-top-level=$(basename "$DIR_SRC") -xzvf "$DIR_FETCH/nettle-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	"$DIR_SRC/configure" --prefix="/usr/local" && \
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	cd "$DIR_BUILD"
	make check
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

