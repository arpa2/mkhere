#!/bin/bash
#
# ARPA2CM build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v1.0.7

GIT_URL="https://gitlab.com/arpa2/arpa2cm.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=build_n_test"

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do_test () {
	# Overrule CTest
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

