#!/bin/bash
#
# Python3 package "build" script -- collects files in PyPi packages
#
# A space-separated list of package names is in $FLAVOUR_pypackages
# and will be used to determine the usual package aspects, such as
# a list of dependencies and a list of file names.  The customary
# packages and pylib packages can be made to retrieve the contents
# and possibly install them.
#
# This mkhere script has an extra subcommand: "pyupdate".  This
# retrieves recent package listings online and installs updates
# for all currently available packages.  Further details depend
# on the underlying package manager setup.
#
# From: Rick van Rein <rick@openfortress.nl>


. $(dirname "$0")/lib/stdlib

default_VERSION 0.0.0


do_touch () {
	echo -n ''
}

do_update () {
	echo -n ''
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo 'python3-dev'
	echo 'libffi-dev'
	echo 'python3-pip'
	echo 'python3-setuptools'
}

do_pydependencies () {
	for PKG in $FLAVOUR_pypackages
	do
		echo $PKG
	done
}

do_check () {
	# Always out of date
	return 1;
}

do_build () {
	#TODO# Implement for Python3
	touch "$DIR_BUILD"
}

do_test () {
	return 0;
}

do_list () {
	#TODO# Implement for Python3
	. $(dirname "$0")/lib/pkglib pip3
	DEPS=$(pkg_dependencies $FLAVOUR_pypackages)
	debug Package Dependencies: $FLAVOUR_pypackages '=>' $DEPS
	for PKG in $FLAVOUR_pypackages $DEPS
	do
		pkg_listfiles $PKG | \
			while read FILE
			do
				# List all but directories
				#TODO# FILE=${FILE#/}
				if [ ! -d "/$FILE" ]
				then
					echo_if_newer_than "$FILE"
				fi
				# Additionally print link targets
				while [ -L "/$FILE" ]
				do
					FILE=$(canon "/$FILE")
					#TODO# FILE=${FILE#/}
					if [ -d "/$FILE" ]
					then
						break
					fi
					echo_if_newer_than "$FILE"
				done
			done
	done | \
	# Remove duplicates caused by link traversal
	unique
}

# The list of OS libraries will be split out per OS package
do_pylibs () {
	SAVE_pypackages="$FLAVOUR_pypackages"
	for PKG in $SAVE_pypackages
	do
		if [ ! -r "$DIR_BUILD/mkhere-pylibs-$PKG.txt" ]
		then
			FLAVOUR_pypackages="$PKG"
			do2_pylibs
			mv "$DIR_BUILD/mkhere-pylibs.txt" \
			   "$DIR_BUILD/mkhere-pylibs-$PKG.txt"
		fi
		cat "$DIR_BUILD/mkhere-pylibs-$PKG.txt"
	done | \
	# Remove duplicates caused by link traversal
	unique
	FLAVOUR_pypackages="$SAVE_pypackages"
}

do_variants () {
	echo -n ''
}

do_flavours () {
	# Not helpful to list all combinations of all packages
	echo -n ''
}

# The extra command for pypackages.sh
do_pyupdate () {
	. $(dirname "$0")/lib/pkglib pip3
	pkg_update
	find /build             -name mkhere-osdeps.txt   -exec rm {} \;
	find /build/pypackages* -name mkhere-osdeps-*.txt -exec rm {} \;
}


main_do_commands "$@"


