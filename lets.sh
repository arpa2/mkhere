#!/bin/bash
#
# LETS build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v1.2
GIT_URL="https://gitlab.com/arpa2/LETS.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
# DOCS_BUILD="doc/html"

do_dependencies () {
	echo arpa2cm
	echo arpa2common
}

do_osdependencies () {
	echo libfcgi
	echo libfcgi-dev
	echo sqlite3
	echo libsqlite3-0
	echo libsqlite3-dev
}

# do2_build () {
# 	do2cmake_build -DfreeDiameter_INCLUDE_DIR=$DESTDIR_freediameter/usr/local/include/freeDiameter -DfreeDiameter_EXECUTABLE=$DESTDIR_freediameter/usr/local/bin/freeDiameterd -DfreeDiameter_EXTENSION_DIR=$DESTDIR_freediameter/usr/local/lib/freeDiameter -DfreeDiameter_fdcore_LIBRARY=$DESTDIR_freediameter/usr/local/lib/libfdcore.so -DfreeDiameter_fdproto_LIBRARY=$DESTDIR_freediameter/usr/local/lib/libfdproto.so -DCONTRIB_CYRUSSASL2_HAAN:BOOL=ON -DCONTRIB_FASTCGI_HAAN:BOOL=ON -DDEBUG:BOOL=ON -DLOG_STYLE:STRING=LOG_STYLE_STDERR #TODO# && \
# 	#TODO# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
# }

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

do_test () {
	do2cmake_test "$@"
}

main_do_commands "$@"

