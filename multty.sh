#!/bin/bash
#
# mulTTY build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

GIT_URL="https://gitlab.com/arpa2/mulTTY.git"

default_VERSION v0.5.3

do_dependencies () {
	echo arpa2cm
	echo arpa2common
	echo quickmem		# to build SASL
	echo quickder		# to build SASL
	echo quicksasl		# to build SASL
}

do_osdependencies () {
	echo python3
	# echo python3-dev
	# echo python3-setuptools
	echo ragel
	echo libpam-dev		# to build SASL
}

do2_build () {
	#OLD# cd "$DIR_SRC" && make -C lib && make -C src
	do2cmake_build #L8R# && \
	#L8R# python3 "$DIR_SRC/setup.py" install --root "$DIR_TREE"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

