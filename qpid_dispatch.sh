#!/bin/bash
#
# Apache Qpid Dispatch build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolcmake

default_VERSION 1.16.0

do_update () {
	cd "$DIR_FETCH"
	empty_dir
	wget "https://github.com/apache/qpid-dispatch/archive/refs/tags/${VERSION}.tar.gz"
	cd "$DIR_SRC"
	empty_dir
	tar --strip-components=1 -xzvf "$DIR_FETCH/${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo 'qpid_proton'
}

do_osdependencies () {
	#OLD# echo libqpid-proton11-dev
	echo python3.9-qpid-proton
	echo python3.9-dev
	echo libsasl2-dev
}

do2_build () {
	# CMAKE_ARGS="-D QPID_DISPATCH_LIB_LINK_FLAGS:STRING=-L/tree/qpid_proton${VERSION_qpid_proton:+-${VERSION_qpid_proton}}/usr/local/lib"
	#ALREADY# export LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+${LD_LIBRARY_PATH}:}/tree/qpid_proton/usr/local/lib"
	# echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
	# export DT_RUNPATH="$LD_LIBRARY_PATH"
	# CMAKE_ARGS="-D QPID_DISPATCH_LIB_LINK_FLAGS:STRING=-rpath-link=/tree/qpid_proton/usr/local/lib"
	# CMAKE_ARGS="-D QPID_DISPATCH_LIB_LINK_FLAGS:STRING=-Wl,-rpath-link,/tree/qpid_proton/usr/local/lib"
	do2cmake_build -DBUILD_TESTING=OFF -DCMAKE_FIND_ROOT_PATH=/tree/qpid_proton
}

do_test () {
	# Skip; there are currently problems with the tests
	# Note: We also set -DBUILD_TESTING=OFF in do2_build()
	return 0
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

