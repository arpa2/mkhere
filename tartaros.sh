#!/bin/bash
#
# tartaros build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v1.0.0

GIT_URL="https://gitlab.com/arpa2/tartaros.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
DOCS_BUILD="doc/html"

do_dependencies () {
	echo 'arpa2cm'
	echo 'arpa2common'
	echo 'quickmem'
	echo 'quickder'
	echo 'quicksasl'
}

do_osdependencies () {
	echo 'ragel'
	echo 'libev-dev'
	echo 'libsasl2-dev'
	# echo 'comerr-dev'
	echo 'doxygen'
	# echo 'graphviz'
	# For testing:
	echo 'sasl2-bin'
	echo 'nginx'
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

