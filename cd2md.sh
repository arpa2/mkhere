#!/bin/bash
#
# cd2md build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION 4041b21b89124996c87b3b047219b31bb79cb1ed

GIT_URL="https://gitlab.com/groengemak/cd2md.git"
# BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
# DOCS_BUILD="doc/html"

do_dependencies () {
	echo quickmem
}

do_osdependencies () {
	echo 'ragel'
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

