#!/bin/bash
#
# arpa2common build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
. $(dirname "$0")/lib/toolcmake

default_VERSION v2.6.2

GIT_URL="https://gitlab.com/arpa2/arpa2common.git"
BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
DOCS_BUILD="doc/html"

do_dependencies () {
	echo 'arpa2cm'
}

do_osdependencies () {
	echo 'comerr-dev'
	echo 'ragel'
	echo 'doxygen'
	echo 'graphviz'
	echo 'liblmdb-dev'
	#NOMORE-SINCE-2.2.7#  echo 'libssl-dev'
	# echo 'libsodium-dev'
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

