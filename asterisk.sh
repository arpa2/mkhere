#!/bin/bash
#
# Asterisk PBX build script -- as a soft switch without hardware connections

#
# The build ends with this output:
#
#
#	 +---- Asterisk Installation Complete -------+
#	 +                                           +
#	 +    YOU MUST READ THE SECURITY DOCUMENT    +
#	 +                                           +
#	 + Asterisk has successfully been installed. +
#	 + If you would like to install the sample   +
#	 + configuration files (overwriting any      +
#	 + existing config files), run:              +
#	 +                                           +
#	 + For generic reference documentation:      +
#	 +    make samples                           +
#	 +                                           +
#	 + For a sample basic PBX:                   +
#	 +    make basic-pbx                         +
#	 +                                           +
#	 +                                           +
#	 +-----------------  or ---------------------+
#	 +                                           +
#	 + You can go ahead and install the asterisk +
#	 + program documentation now or later run:   +
#	 +                                           +
#	 +               make progdocs               +
#	 +                                           +
#	 + **Note** This requires that you have      +
#	 + doxygen installed on your local system    +
#	 +-------------------------------------------+
#	
#

. $(dirname "$0")/lib/stdlib

default_VERSION 18.12.0

do_update () {
	cd "$DIR_FETCH"
	empty_dir
	wget https://downloads.asterisk.org/pub/telephony/asterisk/releases/asterisk-${VERSION}.tar.gz
	cd "$DIR_SRC"
	empty_dir
	tar --strip-components=1 -xzvf "$DIR_FETCH/asterisk-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	true
}

do_osdependencies () {
	echo libedit-dev
	echo libjansson-dev
	echo libxml2-dev
	echo libsrtp2-dev
	echo libbluetooth-dev
	echo libsqlite3-dev
	echo libldap-dev
	echo libogg-dev
	echo libvorbis-dev
	echo libopus-dev
	echo libopusfile-dev
	echo libspeex-dev
	echo libspeexdsp-dev
	echo libcodec2-dev
}

do2_build () {
	empty_dir "$DIR_BUILD"
	cp -r "$DIR_SRC"/* "$DIR_BUILD/"
	cd "$DIR_BUILD"
	./configure \
		--with-pjproject-bundled --with-srtp \
		--with-bluetooth \
		--with-sqlite3 --with-ldap \
		--with-opus --with-opusfile --with-speex --with-ogg --with-vorbis --with-codec2 \
		&&
	touch .cleancount &&
	make &&
	DESTDIR="$DIR_TREE" make install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

