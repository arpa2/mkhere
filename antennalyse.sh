#!/bin/bash
#
# antennalyse build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit
# . $(dirname "$0")/lib/toolcmake

default_VERSION v0.0.1

GIT_URL="https://gitlab.com/PA1RVR/antennalyse.git"
# BIN_URL="${GIT_URL%.git}/-/jobs/artifacts/$VERSION/raw/dist.tgz?job=mkhere"
DOCS_BUILD="site"

do_dependencies () {
	return 0
}

do_osdependencies () {
	echo mkdocs
	echo python3-setuptools
	echo python3-pip
	#TODO# python3 -> markdown-katex
}

do_build () {
	echo '<html><body><b>Documentation without Tests</b></body></html>' > "$DIR_BUILD/test.html"
	pip3 install markdown-katex
	mkdir -p "$DIR_BUILD/site"
	cd "$DIR_SRC"
	echo "Listing DIR_SRC=$DIR_SRC:"
	find . | sed 's/^/ - /'
	#TODO# When the site is complete, add -s for strict mode
	mkdocs build -v -t readthedocs -d "$DIR_BUILD/site"
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

