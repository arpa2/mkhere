#!/bin/bash
#
# MUCH TODO :- freeswitch is "open" but it conceals its complex build instructions
#
# freeswitch build script

. $(dirname "$0")/lib/stdlib
. $(dirname "$0")/lib/toolgit

GIT_URL="https://github.com/signalwire/freeswitch"

default_VERSION v1.10.7

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo autoconf
	echo automake
	echo libtool
}

do2_build () {
	empty_dir "$DIR_BUILD"
	cp -r "$DIR_SRC"/* "$DIR_BUILD/"
	cd "$DIR_BUILD"
	sh bootstrap.sh
	for d in
		libs/libscgi
		libs/libyuv/util
		#TODO#scgi.h# libs/libscgi/perl
		#TODO#ruby.h# libs/esl/ruby
		#TODO#Python.h# libs/esl/python
		#TODO#lua.h# libs/esl/lua
		#TODO#tcl.h# libs/esl/tcl
		#TODO#jni.h# libs/esl/java
		#TODO#esl.h# libs/esl/managed
		#TODO#Python.h# libs/esl/python3
		libs/libvpx/build/make
		libs/libnatpmp
		libs/xmlrpc-c/include
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/xml-rpc-api2cpp
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/xmlrpc_cpp_proxy
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/xmlrpc_transport
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/xmlrpc
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/lib
		#TODO#PERMISSION_DENIED# libs/xmlrpc-c/tools/xmlrpc_pstream
		libs/xmlrpc-c/tools/binmode-rpc-kit
		libs/xmlrpc-c/tools/turbocharger
		#TODO#MISSING_CONFIG.MK# libs/xmlrpc-c/tools
		#TODO#MISSING_CONFIG.MK# libs/xmlrpc-c/examples/cpp
		#TODO#MISSING_CONFIG.MK# libs/xmlrpc-c/examples
		libs/xmlrpc-c/lib/expat/xmlparse
		libs/xmlrpc-c/lib/expat/xmltok
		libs/xmlrpc-c/lib/expat/gennmtab
		libs/xmlrpc-c/lib/expat
		libs/xmlrpc-c/lib/abyss/src
		libs/xmlrpc-c/lib/abyss
		libs/xmlrpc-c/lib/wininet_transport
		libs/xmlrpc-c/lib/curl_transport
		libs/xmlrpc-c/lib/libwww_transport
		libs/xmlrpc-c/lib/libutil
		libs/xmlrpc-c/lib/util
		libs/xmlrpc-c/lib
		libs/xmlrpc-c/src/cpp
		libs/xmlrpc-c/src/cpp/test
		libs/xmlrpc-c/src/test
		libs/xmlrpc-c/src
		libs/xmlrpc-c
		libs/miniupnpc
		#TODO#spandsp.h# scripts/c/socket2me
		#TODO#FILENOTFOUND#dmcs# src/mod/languages/mod_managed/managed
		#TODO#FILENOTFOUND# modmake.rules# src/mod/codecs/mod_yuv
		#TODO#CC-INSTRUCTIONSET_ERROR# src/mod/endpoints/mod_skypopen/old-stuff/asterisk
		#TODO#NOSUCHFILE# src/mod/endpoints/mod_skypopen/oss
		#TODO#asterisk# src/mod/endpoints/mod_gsmopen/asterisk
		#TODO#NOSUCHFILE# src/mod/endpoints/mod_gsmopen/driver_usb_dongle
		#TODO#windos.h# src/mod/endpoints/mod_gsmopen/win_iconv
		#TODO#MAKEFILE_MISSING_SEPARATOR# src/mod/endpoints/mod_gsmopen/gsmlib/gsmlib-1.10-patched-13ubuntu/intl
		#TODO#NOTFOUND# MODMAKE.RULES# src/mod/endpoints/mod_gsmopen/alsa_nogsmlib_nocplusplus/mod_gsmopen
		src/mod/timers/mod_posix_timer/test
	do
		make -C "$d"
	done
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

