#!/bin/bash
#
# knc build script

. $(dirname "$0")/lib/stdlib

default_VERSION 1.7.1

do_update () {
	cd "$DIR_FETCH"
	wget http://oskt.secure-endpoints.com/downloads/knc-${VERSION}.tar.gz
	cd "$DIR_SRC"
	empty_dir "$DIR_SRC/knc-${VERSION}"
	tar --strip-components=1 -xzvf "$DIR_FETCH/knc-${VERSION}.tar.gz"
	do_touch
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo -n ''
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	"$DIR_SRC/configure" --prefix="/usr" && \
	make && \
	empty_dir "$DIR_TREE" && \
	make DESTDIR="$DIR_TREE" install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

