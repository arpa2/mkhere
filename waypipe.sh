#!/bin/bash
#
# waypipe build script (Pass-through for Wayland over connections)

. $(dirname "$0")/lib/stdlib

default_VERSION v0.8.4

do_update () {
	cd "$DIR_SRC"
	empty_dir
	git clone https://gitlab.freedesktop.org/mstoeckl/waypipe "$DIR_GIT"
	cd "$DIR_GIT" ; git reset $VERSION
}

do_dependencies () {
	echo -n ''
}

do_osdependencies () {
	echo meson
	echo ninja-build
	echo pkg-config
	echo python3
}

do2_build () {
	cd "$DIR_BUILD"
	empty_dir
	meson --buildtype debugoptimized "$DIR_BUILD" "$DIR_GIT"
	ninja all && \
	empty_dir "$DIR_TREE" && \
	DESTDIR="$DIR_TREE" ninja install
}

do_test () {
	return 0;
}

do_list () {
	cd "$DIR_TREE"
	find . -type f,l $FIND_NEWER_THAN | sed 's+^\./++'
}

do_variants () {
	echo -n ''
}

do_flavours () {
	echo -n ''
}

main_do_commands "$@"

